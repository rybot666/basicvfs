from io import BytesIO
from VFS.ZipArchive import BufferedZipFile as ZipArchive
from ZipArchive import ZipInfo as Info

def readobj(arc, name):
    obj_rel = arc.open(name)
    obj = BytesIO(obj_rel.read())
    obj_rel.close()
    return obj

def load_fs(self, fn):
    rel_arc = ZipArchive(fn, 'r')

    fns = rel_arc.namelist()
    files = [readobj(rel_arc, fn) for fn in fns]

    rel_arc.close()

    rel_arc = ZipArchive(fn, 'w')
    for f in files: rel_arc.writebuffered(Info(f.name), f)
    del files

    return rel_arc
