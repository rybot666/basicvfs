from VFS.ZipArchive import BufferedZipFile as ZipArchive
from zipfile import ZipInfo as info
from io import BytesIO as VFile
import tempfile
import shutil
import os

class Archive():
    def __init__(self, fn):
        self.fn = fn
        self.arc = ZipArchive(fn ,'w')
        self.ind = None
        self.fail = True

    def close(self):
        print('Closed VFS - {vname}'.format(vname=self.arc.name))
        self.arc.close()

    def exists(self, name):
        for n in self.arc.namelist():
            if n == name:
                return True
        return False

    def write(self, name, obj):
        obj.seek(0)
        self.arc.writebuffered(info(name), obj)
        
    def mkdir(self, name):
        inf = info(name)
        inf.external_attr = 16
        self.arc.writestr(inf, name)
        
    def read(self, name):
        if not self.exists(name): return False
        obj = self.arc.open(name)
        data = obj.read()
        obj.close()
        return data

    def readlines(self, name):
        if not self.exists(name): return False
        obj = self.arc.open(name)
        data = obj.readlines()
        obj.close()
        return data

    def deletefile(self, name):
        if not self.exists(name): return False
        tmp_archive = ZipArchive(tempfile.mkstemp()[1], 'w')
        fns = [x.filename for x in self.arc.infolist() if x.filename != name]
        files = [self.arc.open(f) for f in fns]
        for file in files: tmp_archive.writebuffered(info(file.name), file)
        os.remove(self.fn)
        shutil.copy(tmp_archive, self.fn)
        self.arc = ZipArchive(self.fn, 'r')

    def readobj(self, name):
        if not self.exists(name): return False
        obj_rel = self.arc.open(name)
        obj = VFile(obj_rel.read())
        obj_rel.close()
        return obj

__all__ = [Archive, VFile, ZipArchive]